from typing import Dict
from datetime import datetime

import pandas as pd

from meusterwette.cup.models import Team, Match, Poule, Player, WorldCup
from meusterwette.cup.constants import (
    FINAL_COMPOSITION,
    SEMI_FINAL_COMPOSITION,
    EIGHT_FINAL_COMPOSITION,
    THIRD_PLACE_COMPOSITION,
    QUARTER_FINAL_COMPOSITION,
)
from meusterwette.cup.exceptions import (
    TeamNotFound,
    TeamAlreadyExists,
    MatchAlreadyExists,
)


class CSVImporter:
    """
    Importer to import .csv files
    """

    def __init__(self, cup: WorldCup) -> None:
        self.cup = cup

        self.poule_map: Dict[str, Poule] = {}
        self.team_map: Dict[str, Team] = {}
        self.match_map: Dict[int, Match] = {}
        self.player_map: Dict[str, Player] = {}

    def import_poules_and_teams(self, csv_file: str) -> None:
        """
        Import all Poules and Teams from a csv file
        """
        content = self.__read_csv_as_whole(csv_file)

        for line in content.itertuples(index=False):
            self.__create_poule_object_from_df_line(line)
            self.__create_team_object_from_df_line(line)
        self.__load_bracket_placeholders()

        Poule.objects.bulk_create(self.poule_map.values())
        Team.objects.bulk_create(self.team_map.values())

    def import_players(self, csv_file: str) -> Dict[str, Player]:
        """
        Import all players from a csv file
        """
        content = self.__read_csv_as_whole(csv_file)

        self.__load_team_map()

        for line in content.itertuples(index=False):
            self.__create_player_object(line)

        Player.objects.bulk_create(self.player_map.values())
        return self.player_map

    def import_matches(self, csv_file: str) -> Dict[int, Match]:
        """
        Import all the matches from a csv file
        """
        content = self.__read_csv_as_whole(csv_file)

        self.__load_team_map()

        for line in content.itertuples(index=False):
            self.__create_match_object_from_df_line(line)

        Match.objects.bulk_create(self.match_map.values())
        return self.match_map

    def __read_csv_as_whole(self, csv_file: str) -> pd.DataFrame:
        """
        Start reading the .csv file as a whole
        """
        return pd.read_csv(csv_file, delimiter=",")

    def __create_player_object(self, line: pd.DataFrame) -> Player:
        """
        Creates Player object from dataframe line
        -> Currently no validation!
        """
        if line.player not in self.player_map:
            player = Player(
                name=line.player,
                team=self.__validate_team_into_object(line.team),
                position=line.position,
                # need validation here!
                # age=line.age or None,
                age=datetime.now(),
                # caps=int(line.caps) or None,
                caps=0,
                # goals=line.goals or None,
                goals=0,
                club=line.club or None,
            )
            self.player_map[player.name] = player
        return self.player_map[line.player]

    def __load_bracket_placeholders(self) -> Dict[str, Team]:
        poule = Poule(
            name="X",
            cup=self.cup,
        )
        self.poule_map[poule.name] = poule

        bracket = (
            EIGHT_FINAL_COMPOSITION
            + QUARTER_FINAL_COMPOSITION
            + SEMI_FINAL_COMPOSITION
            + FINAL_COMPOSITION
            + THIRD_PLACE_COMPOSITION
        )

        for match_teams in bracket:
            for team in match_teams:
                team = Team(
                    name=team,
                    poule=poule,
                    cup=self.cup,
                )
                self.team_map[team.name] = team
        return self.team_map

    def __create_team_object_from_df_line(self, line: pd.DataFrame) -> Team:
        """
        Creates Team object from dataframe line
        -> Currently no validation!
        """
        if line.team in self.team_map:
            # Extend the exception, instead of printing it
            print(line, self.team_map)
            raise TeamAlreadyExists
        team = Team(
            name=line.team,
            poule=self.poule_map[line.group],
            cup=self.cup,
        )
        self.team_map[team.name] = team

        return team

    def __create_poule_object_from_df_line(self, line: pd.DataFrame) -> Poule:
        """
        Creates Team object from dataframe line
        -> Currently no validation!
        """
        if line.group not in self.poule_map:
            poule = Poule(name=line.group, cup=self.cup)
            self.poule_map[poule.name] = poule

        return self.poule_map[line.group]

    def __create_match_object_from_df_line(self, line: pd.DataFrame) -> Match:
        """
        Creates Match object from dataframe line
        -> Currently no validation!
        """
        if line.match in self.match_map:
            # Extend the exception, instead of printing it
            print(line, self.match_map)
            raise MatchAlreadyExists

        match = Match(
            number=line.match,
            date=datetime.strptime(line.date, "%d/%m/%Y"),
            home_team=self.__validate_team_into_object(line.country1),
            away_team=self.__validate_team_into_object(line.country2),
            phase=line.phase,
            cup=self.cup,
        )
        self.match_map[match.number] = match

        return match

    def __validate_team_into_object(self, team: str) -> Team:
        """ """
        if team not in self.team_map:
            # Extend the exception, instead of printing it
            print(team, self.team_map)
            raise TeamNotFound
        return self.team_map[team]

    def __load_team_map(self) -> None:
        """
        Load up all in the teams in a dictionary map for match validation
        """
        teams = Team.objects.filter(cup=self.cup)
        for team in teams:
            self.team_map[team.name] = team
