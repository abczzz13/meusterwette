from django.apps import AppConfig


class CupConfig(AppConfig):
    name = "meusterwette.cup"
    label = "cup"
