class TeamNotFound(Exception):
    """Team could not be found"""


class TeamAlreadyExists(Exception):
    """Team already exists"""


class MatchAlreadyExists(Exception):
    """Match already exists"""
