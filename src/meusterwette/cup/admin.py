from django.contrib import admin

from meusterwette.cup.models import Team, Match, Poule, Player, WorldCup

models = [Team, Poule, Player, Match, WorldCup]

for model in models:
    admin.site.register(model)
