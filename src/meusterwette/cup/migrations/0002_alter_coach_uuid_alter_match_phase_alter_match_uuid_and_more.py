# Generated by Django 4.1.2 on 2022-10-29 14:51

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ("cup", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="coach",
            name="uuid",
            field=models.UUIDField(
                default=uuid.uuid4, editable=False, primary_key=True, serialize=False
            ),
        ),
        migrations.AlterField(
            model_name="match",
            name="phase",
            field=models.CharField(
                choices=[
                    ("group matches", "group matches"),
                    ("round 16", "round 16"),
                    ("quarter finals", "quarter finals"),
                    ("semi finals", "semi finals"),
                    ("third place", "third place"),
                    ("final", "final"),
                ],
                max_length=255,
            ),
        ),
        migrations.AlterField(
            model_name="match",
            name="uuid",
            field=models.UUIDField(
                default=uuid.uuid4, editable=False, primary_key=True, serialize=False
            ),
        ),
        migrations.AlterField(
            model_name="player",
            name="position",
            field=models.CharField(
                choices=[("FW", "FW"), ("MF", "MF"), ("DF", "DF"), ("GK", "GK")],
                max_length=255,
            ),
        ),
        migrations.AlterField(
            model_name="player",
            name="uuid",
            field=models.UUIDField(
                default=uuid.uuid4, editable=False, primary_key=True, serialize=False
            ),
        ),
        migrations.AlterField(
            model_name="poule",
            name="name",
            field=models.CharField(max_length=1),
        ),
        migrations.AlterField(
            model_name="poule",
            name="uuid",
            field=models.UUIDField(
                default=uuid.uuid4, editable=False, primary_key=True, serialize=False
            ),
        ),
        migrations.AlterField(
            model_name="team",
            name="uuid",
            field=models.UUIDField(
                default=uuid.uuid4, editable=False, primary_key=True, serialize=False
            ),
        ),
        migrations.AlterField(
            model_name="worldcup",
            name="uuid",
            field=models.UUIDField(
                default=uuid.uuid4, editable=False, primary_key=True, serialize=False
            ),
        ),
        migrations.DeleteModel(
            name="PouleTeams",
        ),
    ]
