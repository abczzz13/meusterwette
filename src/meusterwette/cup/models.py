from django.db import models

from meusterwette.core.models import BaseModel
from meusterwette.cup.constants import PHASES, POSITIONS


class WorldCup(BaseModel):
    name = models.CharField(max_length=255)
    year = models.PositiveIntegerField()


class Poule(BaseModel):
    name = models.CharField(max_length=1)
    cup = models.ForeignKey(WorldCup, on_delete=models.CASCADE)


class Team(BaseModel):
    name = models.CharField(max_length=255)
    poule = models.ForeignKey(Poule, on_delete=models.CASCADE)
    cup = models.ForeignKey(WorldCup, on_delete=models.CASCADE)


class Match(BaseModel):
    number = models.IntegerField()
    date = models.DateField()
    home_team = models.ForeignKey(
        Team, related_name="match_home_team", on_delete=models.CASCADE
    )
    away_team = models.ForeignKey(
        Team, related_name="match_away_team", on_delete=models.CASCADE
    )
    phase = models.CharField(max_length=255, choices=PHASES)
    cup = models.ForeignKey(WorldCup, on_delete=models.CASCADE)


class Player(BaseModel):
    name = models.CharField(max_length=255)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    position = models.CharField(max_length=255, choices=POSITIONS)
    age = models.DateField(blank=True, null=True)
    caps = models.PositiveIntegerField(blank=True, default=0)
    goals = models.PositiveIntegerField(blank=True, default=0)
    club = models.CharField(max_length=255, blank=True)
