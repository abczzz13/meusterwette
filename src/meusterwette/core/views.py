from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet


class HealthViewSet(ViewSet):
    """ViewSet for the health endpoint of the API"""

    def simple(self, request: Request) -> Response:
        """
        Always return a 200 response as token of life.
        """
        return Response()
