from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = "meusterwette.core"
    label = "core"
