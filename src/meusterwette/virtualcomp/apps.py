from django.apps import AppConfig


class VirtualcompConfig(AppConfig):
    name = "meusterwette.virtualcomp"
    label = "virtualcomp"
