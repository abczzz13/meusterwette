from django.contrib import admin

from meusterwette.virtualcomp.models import Participant, ParticipantPlayer

admin.site.register(Participant)
admin.site.register(ParticipantPlayer)
