from django.db import models

from meusterwette.cup.models import Match, Player
from meusterwette.core.models import BaseModel


class Participant(BaseModel):
    name = models.CharField(max_length=255)
    email = models.EmailField(blank=False)


class ParticipantPlayer(BaseModel):
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    start_game = models.PositiveIntegerField()
    end_game = models.PositiveIntegerField()


class Score(BaseModel):
    # participant = models.ForeignKey(Participant, on_delete=models.CASCADE)
    participant_player = models.ForeignKey(ParticipantPlayer, on_delete=models.CASCADE)
    description = models.CharField(max_length=255)
    points = models.IntegerField()
    match = models.ForeignKey(Match, on_delete=models.CASCADE)


# class ParticipantTeam(BaseModel):
# ParticipantPlayers:
# 1 keeper
# 4 defenders
# 3 midfielders
# 3 attackers

# max 3 players from same country/team
