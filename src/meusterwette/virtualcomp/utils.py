from typing import Dict, List
from functools import partial

from meusterwette.cup.models import Match
from meusterwette.stats.models import Card, Goal, Penalty
from meusterwette.cup.constants import COACH, KEEPER, ATTACKER, DEFENDER, MIDFIELDER
from meusterwette.stats.constants import RED_CARD, YELLOW_CARD, YELLOW_CARD_SECOND
from meusterwette.stats.exceptions import MatchNotFound
from meusterwette.virtualcomp.models import Score, ParticipantPlayer
from meusterwette.virtualcomp.constants import (
    WIN,
    DRAW,
    LOSS,
    KEEP_ZERO,
    POINTS_WIN,
    POINTS_DRAW,
    POINTS_LOSS,
    SCORED_GOAL,
    PENALTY_GOAL,
    PENALTY_MISS,
    STOP_PENALTY,
    DID_NOT_SCORE,
    POINTS_RED_CARD,
    POINTS_YELLOW_CARD,
    POINTS_GK_KEEP_ZERO,
    POINTS_PENALTY_GOAL,
    POINTS_PENALTY_MISS,
    POINTS_DF_SCORES_GOAL,
    POINTS_FW_SCORES_GOAL,
    POINTS_GK_SCORES_GOAL,
    POINTS_MF_SCORES_GOAL,
    WIN_NO_CONCEDED_GOALS,
    POINTS_GK_STOP_PENALTY,
    WIN_AND_TWO_GOALS_PLUS,
    CONCEDED_TWO_GOALS_PLUS,
    POINTS_FW_DID_NOT_SCORE,
    POINTS_MF_DID_NOT_SCORE,
    POINTS_SECOND_YELLOW_CARD,
    POINTS_DF_WIN_NO_CONCEDED_GOALS,
    POINTS_CO_WIN_AND_TWO_GOALS_PLUS,
    POINTS_FW_WIN_AND_TWO_GOALS_PLUS,
    POINTS_MF_WIN_AND_TWO_GOALS_PLUS,
    POINTS_CO_CONCEDED_GOALS_TWO_PLUS,
    POINTS_DF_CONCEDED_GOALS_TWO_PLUS,
    POINTS_GK_CONCEDED_GOALS_TWO_PLUS,
)


class MatchResultProcessor:
    def __init__(self, match: int):
        match_object = (
            Match.objects.filter(number=match)
            # .select_related("goals", "penalties", "cards")
            .first()
        )
        if not match_object:
            raise MatchNotFound

        self.match = match_object
        self.goals = Goal.objects.filter(match=self.match)
        self.penalties = Penalty.objects.filter(match=self.match)
        self.cards = Card.objects.filter(match=self.match)

        self.match_team_map: Dict[str, str] = {
            self.match.home_team: self.match.away_team,
            self.match.away_team: self.match.home_team,
        }
        self.match_goals_map: Dict[str, int] = self.__retrieve_goals()
        self.match_outcomes_map: Dict[str, str] = self.__define_match_outcome()

        self.scores: List[Score] = []

    def __retrieve_goals(self) -> Dict[str, int]:
        goals_map = {
            f"{self.match.home_team.name}": 0,
            f"{self.match.away_team.name}": 0,
        }

        for goal in self.goals:
            if (
                goal.team == self.match.home_team
                and not goal.own_goal
                or goal.team == self.match.away_team
                and goal.own_goal
            ):
                goals_map[self.match.home_team.name] += 1
            else:
                goals_map[self.match.away_team.name] += 1
        return goals_map

    def __define_match_outcome(self) -> Dict[str, str]:
        outcome_map = {}
        home_goals = self.match_goals_map[self.match.home_team.name]
        away_goals = self.match_goals_map[self.match.away_team.name]

        if home_goals == away_goals:
            for team in self.match_team_map.keys():
                outcome_map[team] = DRAW
        else:
            is_home_winner = home_goals > away_goals
            outcome_map[self.match.home_team.name] = WIN if is_home_winner else LOSS
            outcome_map[self.match.away_team.name] = LOSS if is_home_winner else WIN
        return outcome_map

    def process_match_results(self):
        """ """
        match_teams = [self.match.home_team, self.match.away_team]
        participant_players = ParticipantPlayer.objects.filter(
            player__team__in=match_teams,
            start_game__lte=self.match.number,
            end_game__gte=self.match.number,
        )
        # take into account switches to the participants team
        # Probably better way to get these objects..

        for player in participant_players:

            self.__process_outcome_scores(player)
            self.__process_card_scores(player)
            self.__process_penalty_scores(player)
            # do something with "meespelen"

            position_map = {
                COACH: self.__process_coach_scores,
                ATTACKER: self.__process_attacker_scores,
                MIDFIELDER: self.__process_midfielder_scores,
                DEFENDER: self.__process_defender_scores,
                KEEPER: self.__process_keeper_scores,
            }

            position_map[player.player.position](player)

        Score.objects.bulk_create(self.scores)

    def __process_outcome_scores(self, player: ParticipantPlayer) -> None:
        outcome_point_map = {
            WIN: partial(self.__add_score, player, WIN, POINTS_WIN),
            LOSS: partial(self.__add_score, player, LOSS, POINTS_LOSS),
            DRAW: partial(self.__add_score, player, DRAW, POINTS_DRAW),
        }
        outcome_point_map[self.match_outcomes_map[player.player.team.name]]()

    def __process_card_scores(self, player: ParticipantPlayer) -> None:
        card_point_map = {
            YELLOW_CARD: partial(
                self.__add_score, player, YELLOW_CARD, POINTS_YELLOW_CARD
            ),
            YELLOW_CARD_SECOND: partial(
                self.__add_score,
                player,
                YELLOW_CARD_SECOND,
                POINTS_SECOND_YELLOW_CARD,
            ),
            RED_CARD: partial(self.__add_score, player, RED_CARD, POINTS_RED_CARD),
        }
        if cards := self.cards.filter(player=player.player):
            for card in cards:
                card_point_map[player.player.name]()

    def __process_penalty_scores(self, player: ParticipantPlayer) -> None:
        penalty_point_map = {
            True: partial(self.__add_score, player, PENALTY_GOAL, POINTS_PENALTY_GOAL),
            False: partial(self.__add_score, player, PENALTY_MISS, POINTS_PENALTY_MISS),
        }
        if penalties := self.penalties.filter(player=player.player):
            for penalty in penalties:
                penalty_point_map[penalty.goal is not None]()

    def __process_coach_scores(self, player: ParticipantPlayer) -> None:
        if self.match_goals_map[self.match_team_map[player.player.team.name]] >= 2:
            self.__add_score(
                player,
                CONCEDED_TWO_GOALS_PLUS,
                POINTS_CO_CONCEDED_GOALS_TWO_PLUS,
            )
        if (
            self.match_outcomes_map[player.player.team.name] == WIN
            and self.match_goals_map[player.player.team.name] >= 2
        ):
            self.__add_score(
                player, WIN_AND_TWO_GOALS_PLUS, POINTS_CO_WIN_AND_TWO_GOALS_PLUS
            )

    def __process_attacker_scores(self, player: ParticipantPlayer) -> None:
        if self.match_goals_map[player.player.team.name] == 0:
            self.__add_score(player, DID_NOT_SCORE, POINTS_FW_DID_NOT_SCORE)
        else:
            player_goals = self.goals.filter(player=player.player)
            for goal in player_goals:
                self.__add_score(player, SCORED_GOAL, POINTS_FW_SCORES_GOAL)

        if (
            self.match_outcomes_map[player.player.team.name] == WIN
            and self.match_goals_map[player.player.team.name] >= 2
        ):
            self.__add_score(
                player, WIN_AND_TWO_GOALS_PLUS, POINTS_FW_WIN_AND_TWO_GOALS_PLUS
            )

    def __process_midfielder_scores(self, player: ParticipantPlayer) -> None:
        if self.match_goals_map[player.player.team.name] == 0:
            self.__add_score(player, DID_NOT_SCORE, POINTS_MF_DID_NOT_SCORE)
        else:
            player_goals = self.goals.filter(player=player.player.name)
            for goal in player_goals:
                self.__add_score(player, SCORED_GOAL, POINTS_MF_SCORES_GOAL)

        if (
            self.match_outcomes_map[player.player.team.name] == WIN
            and self.match_goals_map[player.player.team.name] >= 2
        ):
            self.__add_score(
                player, WIN_AND_TWO_GOALS_PLUS, POINTS_MF_WIN_AND_TWO_GOALS_PLUS
            )

    def __process_defender_scores(self, player: ParticipantPlayer) -> None:
        if (
            self.match_goals_map[self.match_team_map[player.player.team.name]] == 0
            and self.match_outcomes_map[player.player.team.name] == WIN
        ):
            self.__add_score(
                player, WIN_NO_CONCEDED_GOALS, POINTS_DF_WIN_NO_CONCEDED_GOALS
            )
        if self.match_goals_map[self.match_team_map[player.player.team.name]] >= 2:
            self.__add_score(
                player,
                CONCEDED_TWO_GOALS_PLUS,
                POINTS_DF_CONCEDED_GOALS_TWO_PLUS,
            )

            player_goals = self.goals.filter(player=player.player.name)
            for goal in player_goals:
                self.__add_score(player, SCORED_GOAL, POINTS_DF_SCORES_GOAL)

    def __process_keeper_scores(self, player: ParticipantPlayer) -> None:
        if self.match_goals_map[self.match_team_map[player.player.team.name]] == 0:
            self.__add_score(player, KEEP_ZERO, POINTS_GK_KEEP_ZERO)

        # not taking into account if the keeper is also keeping during the match
        if self.penalties.filter(
            team=self.match_team_map[player.player.team.name],
            goal=None,
        ):
            self.__add_score(player, STOP_PENALTY, POINTS_GK_STOP_PENALTY)

        if self.match_goals_map[self.match_team_map[player.player.team.name]] >= 2:
            self.__add_score(
                player,
                CONCEDED_TWO_GOALS_PLUS,
                POINTS_GK_CONCEDED_GOALS_TWO_PLUS,
            )

        player_goals = self.goals.filter(player=player.player.name)
        for goal in player_goals:
            self.__add_score(player, SCORED_GOAL, POINTS_GK_SCORES_GOAL)

    def __add_score(self, player: ParticipantPlayer, description: str, points: int):
        score = Score(
            participant_player=player,
            description=description,
            points=points,
            match=self.match,
        )
        print(score.__dict__)
        self.scores.append(score)
