from django.apps import AppConfig


class StatsConfig(AppConfig):
    name = "meusterwette.stats"
    label = "stats"
