from typing import Dict, List, Union

from meusterwette.cup.models import Team, Match, Player
from meusterwette.stats.models import Card, Goal, Penalty
from meusterwette.stats.exceptions import InvalidTeam, InvalidPlayer, MatchNotFound


class MatchResultImporter:
    def __init__(self, match: int) -> None:
        match_object = (
            Match.objects.filter(number=match)
            .select_related("home_team", "away_team")
            .first()
        )
        if not match_object:
            raise MatchNotFound

        self.match = match_object
        self.player_map: Dict[str, Player] = {}
        self.team_map: Dict[str, Team] = {}
        self.goal_map: Dict[str, List[Goal]] = {}
        self.penalty_map: Dict[str, List[Penalty]] = {}
        self.card_map: Dict[str, List[Card]] = {}

        self.__create_team_map()
        self.__create_player_map()

    def process_match_result(self, data) -> None:

        for goal in data["goals"]:
            goal_object = Goal(
                player=self.__validate_player_into_object(goal["player"]),
                team=self.__validate_team_into_object(goal["team"]),
                match=self.match,
                own_goal=goal["own_goal"],
            )
            if goal["player"] in self.goal_map:
                self.goal_map[goal["player"]].append(goal_object)
            else:
                self.goal_map[goal["player"]] = [goal_object]

        for penalty in data["penalties"]:
            penalty_object = Penalty(
                player=self.__validate_player_into_object(penalty["player"]),
                team=self.__validate_team_into_object(penalty["team"]),
                match=self.match,
                goal=self.__goal_from_penalty(penalty["goal"], penalty["player"]),
            )
            if penalty["player"] in self.penalty_map:
                self.penalty_map[penalty["player"].append(penalty_object)]
            else:
                self.penalty_map[penalty["player"]] = [penalty_object]

        for card in data["cards"]:
            card_object = Card(
                player=self.__validate_player_into_object(card["player"]),
                team=self.__validate_team_into_object(card["team"]),
                match=self.match,
                type=card["type"],
            )
            if card["player"] in self.card_map:
                self.card_map[card["player"]].append(card_object)
            else:
                self.card_map[card["player"]] = [card_object]

        Goal.objects.bulk_create(self.__unpack_map(self.goal_map))
        Penalty.objects.bulk_create(self.__unpack_map(self.penalty_map))
        Card.objects.bulk_create(self.__unpack_map(self.card_map))

    def __validate_team_into_object(self, team: str) -> Team:
        if team not in self.team_map:
            raise InvalidTeam
        return self.team_map[team]

    def __validate_player_into_object(self, player: str) -> Player:
        if player not in self.player_map:
            raise InvalidPlayer
        return self.player_map[player]

    def __goal_from_penalty(self, is_goal: bool, player: str) -> Union[Goal, None]:
        return self.goal_map[player][0] if is_goal else None  # need to change this...

    def __create_player_map(self) -> None:
        players = Player.objects.filter(team__name__in=self.team_map)
        for player in players:
            self.player_map[player.name] = player

    def __create_team_map(self) -> None:
        for team in (self.match.home_team, self.match.away_team):
            self.team_map[team.name] = team

    def __unpack_map(self, object_map) -> List[Goal]:
        object_list = []
        for objects in object_map.values():
            object_list.extend(objects)
        return object_list
