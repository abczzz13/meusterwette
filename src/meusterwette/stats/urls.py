from rest_framework.routers import DefaultRouter

from meusterwette.stats.views import MatchResultViewSet

app_name = "stats"

router = DefaultRouter()
router.register("matchresult", MatchResultViewSet, basename="matchresult")

urlpatterns = router.urls
