from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from meusterwette.stats.utils import MatchResultImporter
from meusterwette.stats.serializers import MatchResultCreateSerializer


class MatchResultViewSet(ViewSet):
    def create(self, request: Request) -> Response:
        serializer = MatchResultCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data

        importer = MatchResultImporter(data["match"])
        importer.process_match_result(data)

        return Response(status=201)
