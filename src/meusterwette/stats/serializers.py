from rest_framework import serializers

from meusterwette.stats.constants import CARDS


class GoalSerializer(serializers.Serializer):  # type: ignore
    player = serializers.CharField()
    team = serializers.CharField()
    own_goal = serializers.BooleanField()
    penalty = serializers.BooleanField()


class CardSerializer(serializers.Serializer):  # type: ignore
    player = serializers.CharField()
    team = serializers.CharField()
    type = serializers.ChoiceField(choices=CARDS)


class PenaltySerializer(serializers.Serializer):  # type: ignore
    player = serializers.CharField()
    team = serializers.CharField()
    goal = serializers.BooleanField()


class MatchResultCreateSerializer(serializers.Serializer):  # type: ignore
    match = serializers.IntegerField()
    goals = GoalSerializer(many=True, required=False)
    penalties = PenaltySerializer(many=True, required=False)
    cards = CardSerializer(many=True, required=False)
