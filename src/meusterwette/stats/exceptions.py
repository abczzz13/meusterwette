class MatchNotFound(Exception):
    """Match could not be found"""


class InvalidTeam(Exception):
    """Team not part of the Match"""


class InvalidPlayer(Exception):
    """Player not part of the Match"""
