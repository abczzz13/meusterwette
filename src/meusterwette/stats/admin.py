from django.contrib import admin

from meusterwette.stats.models import Card, Goal, Penalty, MatchResult

admin.site.register(Goal)
admin.site.register(Card)
admin.site.register(Penalty)
admin.site.register(MatchResult)
