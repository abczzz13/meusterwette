from django.db import models

from meusterwette.cup.models import Team, Match, Player
from meusterwette.core.models import BaseModel
from meusterwette.stats.constants import CARDS


class Goal(BaseModel):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    own_goal = models.BooleanField(default=False)


class Penalty(BaseModel):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    goal = models.ForeignKey(Goal, null=True, on_delete=models.CASCADE)


class Card(BaseModel):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    type = models.CharField(max_length=255, choices=CARDS)


class MatchResult(BaseModel):
    match = models.ForeignKey(Match, on_delete=models.CASCADE)

    @property
    def goals(self) -> models.QuerySet[Goal]:
        """
        Returns the goals of the match
        """
        return Goal.objects.filter(match=self.match)

    @property
    def score(self) -> str:
        """
        Returns the score of the match
        """
        home_goals, away_goals = 0, 0
        goals = Goal.objects.filter(
            match=self.match,
        )
        for goal in goals:
            if self.is_home_team_goal(goal):
                home_goals += 1

            if self.is_away_team_goal(goal):
                away_goals += 1

        return (
            f"{self.match.home_team} {home_goals} - {away_goals} {self.match.away_team}"
        )

    def is_home_team_goal(self, goal: Goal) -> bool:
        return (
            goal.team == self.match.home_team
            and not goal.own_goal
            or goal.team == self.match.away_team
            and goal.own_goal
        )

    def is_away_team_goal(self, goal: Goal) -> bool:
        return (
            goal.team == self.match.away_team
            and not goal.own_goal
            or goal.team == self.match.home_team
            and goal.own_goal
        )

    '''
    @property
    def score(self) -> str:
        """
        Returns the score of the match
        """
        home_goals = Goal.objects.filter(
            match=self.match,
            team=self.match.home_team,
            own_goal=False,  # how to take into account own goals?
        ).count()
        away_goals = Goal.objects.filter(
            match=self.match,
            team=self.match.away_team,
            own_goal=False,  # how to take into account own goals?
        ).count()
        return (
            f"{self.match.home_team} {home_goals} - {away_goals} {self.match.away_team}"
        )
    '''
