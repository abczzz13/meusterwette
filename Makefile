clean:
	docker-compose down --remove-orphans -v

run: kill build
	docker-compose up -d app

shell: kill build
	docker-compose up app python src/meusterwette/manage.py shell_plus --print-sql

stop:
	docker-compose down

kill:
	docker-compose kill app
	docker-compose down

build:
	docker-compose build app

build-tox:
	docker-compose build tox

pull_cache:
	docker-compose pull app

pull_tox_cache:
	docker-compose pull tox

push: push_tox
	docker-compose push app

push_tox:
	docker-compose push tox

pre-commit:
	pre-commit run --all-files

test:
	pytest --isort --flake8 --black --cov=src
	# pytest --cov-config=coverage.ini --isort --flake8 --black --cov=src

tox: kill build-tox
	docker-compose run --rm tox

tox/%: kill build-tox
	docker-compose run --rm --service-ports tox --test tests/$*
