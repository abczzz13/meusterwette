import pytest
from django.urls import reverse
from rest_framework.test import APIClient

from meusterwette.cup.models import Player
from meusterwette.virtualcomp.utils import MatchResultProcessor
from meusterwette.virtualcomp.models import Score, Participant, ParticipantPlayer


@pytest.mark.django_db
class TestMatchResultProcessor:
    def test_matchresult_processor(self, api_client: APIClient, cup_data) -> None:
        url = reverse("stats:matchresult-list")
        data = dict(
            match=2,
            goals=[
                dict(
                    player="Memphis Depay",
                    team="Netherlands",
                    own_goal=False,
                    penalty=False,
                ),
                dict(
                    player="Memphis Depay",
                    team="Netherlands",
                    own_goal=False,
                    penalty=False,
                ),
            ],
            cards=[
                dict(
                    player="Daley Blind",
                    team="Netherlands",
                    type="Yellow Card",
                ),
            ],
            penalties=[
                dict(
                    player="Memphis Depay",
                    team="Netherlands",
                    goal=False,
                ),
            ],
        )

        api_client.post(url, data, format="json")

        participant = Participant.objects.create(
            name="Test User",
            email="test_user@mail.com",
        )
        player = Player.objects.get(name="Memphis Depay")
        ParticipantPlayer.objects.create(
            participant=participant,
            player=player,
            start_game=0,
            end_game=62,
        )

        processor = MatchResultProcessor(match=2)
        processor.process_match_results()

        assert Score.objects.count() == 5

        assert False
