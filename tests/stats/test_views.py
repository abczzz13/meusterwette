import pytest
from django.urls import reverse
from rest_framework.test import APIClient

from meusterwette.stats.models import Card, Goal, Penalty


@pytest.mark.django_db
class TestMatchResultViewSet:
    def test_post_match_result(self, api_client: APIClient, cup_data):
        url = reverse("stats:matchresult-list")
        data = dict(
            match=2,
            goals=[
                dict(
                    player="Memphis Depay",
                    team="Netherlands",
                    own_goal=False,
                    penalty=False,
                ),
                dict(
                    player="Memphis Depay",
                    team="Netherlands",
                    own_goal=False,
                    penalty=False,
                ),
            ],
            cards=[
                dict(
                    player="Daley Blind",
                    team="Netherlands",
                    type="Yellow Card",
                ),
            ],
            penalties=[
                dict(
                    player="Memphis Depay",
                    team="Netherlands",
                    goal=False,
                ),
            ],
        )

        api_client.post(url, data, format="json")

        assert Goal.objects.count() == 2
        assert Penalty.objects.count() == 1
        assert Card.objects.count() == 1

        assert False
