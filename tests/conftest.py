import pytest
from django.conf import settings
from rest_framework.test import APIClient

from meusterwette.cup.utils import CSVImporter
from meusterwette.cup.models import WorldCup


@pytest.fixture
def api_client():
    """
    Fixture to provide the API client
    """
    return APIClient()


@pytest.mark.django_db(True)
@pytest.fixture
def world_cup():
    """
    Fixture to get a WorldCup object
    """
    cup = WorldCup(name="WC Qatar", year=2022)
    cup.save()
    return cup


@pytest.mark.django_db(True)
@pytest.fixture
def cup_data(world_cup):
    importer = CSVImporter(world_cup)

    importer.import_poules_and_teams(settings.POULE_CSV_FILE)
    importer.import_matches(settings.MATCH_CSV_FILE)
    importer.import_players(settings.PLAYER_CSV_FILE)
